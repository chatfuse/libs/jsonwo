#include "jsonwo.hpp"

#include <iomanip>
#include <string>
#include <sstream>
#include <vector>
#include <utility>



namespace JsonWO {
std::string f(std::string_view value) {
    std::ostringstream fress;
    fress << '"';
    for (const auto c : value) {
        switch (c) {
        case '"': fress << "\\\""; break;
        case '\\': fress << "\\\\"; break;
        case '\b': fress << "\\b"; break;
        case '\f': fress << "\\f"; break;
        case '\n': fress << "\\n"; break;
        case '\r': fress << "\\r"; break;
        case '\t': fress << "\\t"; break;
        default:
            if ('\x00' <= c && c <= '\x1f') {
                fress << "\\u"
                      << std::hex << std::setw(4) << std::setfill('0') << static_cast<int>(c);
            } else {
                fress << c;
            }
        }
    }
    fress << '"';
    return fress.str();
}

std::string f(std::vector<std::string_view> value) {
    std::string fres;
    {
        std::ostringstream fress;
        fress << '[';
        for (const auto& item : value) {
            fress << item << ',';
        }
        fres = fress.str();
    }
    fres[fres.size()-1] = ']';
    return fres;
}

std::string f(std::vector<std::pair<std::string_view, std::string_view> > value) {
    std::string fres;
    {
        std::ostringstream fress;
        fress << '{';
        for (const auto& item : value) {
            fress << item.first << ':' << item.second << ',';
        }
        fres = fress.str();
    }
    fres[fres.size()-1] = '}';
    return fres;
}
}
