#ifndef _JSONWO_HPP
#define _JSONWO_HPP
#include <string>
#include <string_view>
#include <vector>
#include <utility>
#include <concepts>



namespace JsonWO {
constexpr inline
std::string_view f() {
    return "null";
}

inline std::string f(std::integral auto value) {
    return std::to_string(value);
}

std::string f(std::string_view value);

std::string f(std::vector<std::string_view> value);

std::string f(std::vector<std::pair<std::string_view, std::string_view>> value);

template<bool value>
constexpr inline
std::string_view f() {
    return value ? "true" : "false";
}
}
#endif
