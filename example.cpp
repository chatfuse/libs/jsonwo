#include "jsonwo.hpp"

#include <iostream>



int main() {
    using namespace JsonWO;
    auto data = f({
                      {
                          f("values"), f({
                              f("I'd say: \"It's working!\""),
                              f("Does not work\"")
                          })
                      },
                      {f("index"), f(0)},
                      {f("valid"), f<true>()}
                  });
    std::cout << data << std::endl;
}
