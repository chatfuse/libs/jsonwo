cmake_minimum_required(VERSION 3.5)

project(jsonwo LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_library(jsonwo jsonwo.cpp)
target_include_directories(jsonwo PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")

add_executable(example example.cpp)
target_link_libraries(example PRIVATE jsonwo)
